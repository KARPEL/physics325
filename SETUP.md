# Lab Setup Guide

## General

The high-voltage power supplies for the PMTs use special white BNC cables.

## Specific Labs

### Michelson Interferometers

The Michelson interferometers that aren't on stands are quite low.
They should be put on a box or a stack of books to raise them up, making them more comfortable to use.

### Fabry-Perot Interferometers

The correct order of optical elements on the rail is FP --> LENS --> TUBE --> PMT.
The lens tube should be placed so that the actual lens is away from the FP, but the opposite end of the tube almost touches the end of the FP cavity.

### Diffraction Gratings

Three/four-jaw mounts are good for holding the calibration lasers.

## Fourier Optics

Put the spatial filters on horizontal/vertical translation stages so that you have many degrees of freedom to align them with.
Getting a reasonable alignment on the spatial filter is critical to having enough light to see what you're doing later in the lab.

## Diffraction

Use the PD10A photodiodes, **not** the PD36A.
You need the small detector area to resolve features, even though the signal is very small.
(This may change in the future if we get irises for the photodiodes).

## AOMs

Students will likely be unfamiliar with how to set up the trigger on the oscilloscope.
Be prepared to mostly do it for them.

## Fiber Optics

Do not bend the fibers too tightly, or they'll break, and everyone will be sad.
