\documentclass[]{manual}

\title{Michelson Interferometers}
\date{\today}
\course{Physics 325}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section{Motivation}

    The Michelson interferometer was one of the first devices that allowed the precise measurement of the wavelength of light.
    Since the wavelength of light is a measure of its energy, interferometers are powerful tools for learning about atomic structure.
    Although modern interferometers are often more useful (such as the Fabry-Perot interferometer), the Michelson is by far the easiest to align and use.
    In this lab we will use a Michelson interferometer to measure the wavelength of light from a sodium lamp, discover that the sodium emission line is a doublet, measure the doublet spacing, and then apply the same technique to a hydrogen/deuterium gas mixture to measure the proton-electron mass ratio.

    \section{Background}

    The Michelson interferometer, like all interferometers, is based on the fact that light can interfere with itself.
    The observed intensity of light is proportional to the square of the amplitude of the electric field.
    If the electric field is composed of sinusoidal components with different phases or wavelengths, we must add them together \textit{before} we square the field.
    This produces regions of high and low intensity that are called \textbf{interference fringes}.

    In a Michelson interferometer (shown in Figure~\ref{fig:michelson-top-down}), a beam of light is passed through a half-silvered mirror, which acts as a beam splitter (BS).
    The reflected and transmitted components of the beam both reflect off mirrors (M1 and M2) and return to the half-silvered mirror, which now recombines them into a single output beam that travels to your eye, as shown below.
    Any difference between the distances from the beam splitter to the mirrors produces a phase shift proportional to the difference in distances traveled.

    \begin{figure}
        \centering

        \begin{tikzpicture}[thick, scale = 1]
            % mirrors
            \draw[pattern = north west lines] (-.5,3.85) rectangle ++(1,.3) node[above, right] {M1};
            \draw[pattern = north west lines, rotate around = {-20:(3,0)}] (2.85,-.5) rectangle ++(.3, 1) node[above, right] {M2};

            % d
            \draw[dashed] (-.5, 3) -- (.5, 3);
            \draw[dashed, |<->|] (-.5,3) -- (-.5,3.85) node[midway, left] {$d$};
            %\node[left] at (-.5, 3.4) {$d$};

            % bs
            \draw[pattern = north west lines, rotate around={-45:(0,0)}] (-.1, -1) rectangle (.1, 1) node[above, right] {BS};
            %\draw (-1, -1) -- (1, 1) node[above, right] {BS};

            % light source
            \draw (-4, -.5) rectangle ++(1.5,1) node[midway] {Lamp};

            % light rays
            \draw[neararrow={stealth}] (-2.5, 0) -- (0,0); % leaving lamp
            \draw[fararrow={stealth}] (0,0) -- (0, -2.43); % going to eye

            \draw[fararrow={stealth}] (0,0) -- (0, 3.85); % going to M1
            \draw[neararrow={latex reversed}] (0,0) -- (0, 3.85); % coming back

            \draw[fararrow={stealth}] (0,0) -- (2.85, 0); % going to M2
            \draw[neararrow={latex reversed}] (0,0) -- (2.85, 0); % coming back

            % eye
            \eye{.75}{0}{-3}{90}
            \node[below] at (0, -3) {Observer};
        \end{tikzpicture}

        \caption{A top-down view of the geometry of a Michelson interferometer.}

         \label{fig:michelson-top-down}
    \end{figure}

    In this figure \ref{fig:michelson-top-down}, M1 is the movable mirror, and its distance from the zero path length difference configuration (where the path length in each ``arm'' of the interferometer is the same) is $d$.  Since the light is reflected back the way it came the total path length difference that the observer sees will end up being $2d$.
    M2 is a tiltable mirror (tilt exaggerated on the diagram), with knobs that adjust the orientation (but not position) of the mirror.

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[dashed] (0, 0) -- (12, 0) node[centered, right] {I$_0$};
            \draw (12, -1) -- (12, 3) node[above, font = \footnotesize] {Observer};

            \draw (6 + 2, -0.5) node[below] {M1} -- (6 + 2, 0.5);
            \draw (7.5 + 2, -0.5) node[below] {M2} -- (7.5 + 2, 0.5);
            \draw[dashed, |<->|] (6 + 2, 0.2) -- (7.5 + 2, 0.2) node[midway, above] {$d$};

            \draw (5.75, -0.5) -- (6.75, 0.5) node[below] at (6.25, -0.5) {BS};

            \draw[midarrow=latex] (0, 0) node[below] {S1} -- (12, 2.5);
            \draw[midarrow=latex] (3, 0) node[below] {S2} -- (12, 2.5);

            \draw[dashed] (3,0) -- ++(101.77:0.612);

            \draw[dashed, |<->|] (0,-0.7) -- (3,-0.7) node[midway, below] {$2d$};

            \fill (0, 0) circle [radius=2pt];
            \fill (3, 0) circle [radius=2pt];

            \draw[dashed, |<->|] (0 - .1,0 + .2) -- ++(11.77:3) node[midway, above, rotate=11.77] {$2d\cos{\theta}$};

            \draw[domain=0:11.77] plot ({1.5*cos(\x)}, {1.5*sin(\x)});
            \node[right] at (1.75, .2) {$\theta$};

            \draw [domain=0:15.52] plot ({3+1.5*cos(\x)}, {1.5*sin(\x)});
            \node[right] at (4.5, .25) {$\approx\theta$};
        \end{tikzpicture}

        \caption{A diagram showing the ``unwrapped'' geometry of the Michelson interferometer.}

        \label{fig:michelson-unwrapped}
    \end{figure}

    To determine what we see when we look down the interferometer, we need to ``unwrap'' the optical system into an equivalent linear system. Consider the light rays arriving at our eyes from one of the mirrors.  Our eyes do not register each change in direction of the rays.  Instead, our eyes perceive the light coming from the lamp in a straight line. We can model this behavior by simply tracing the rays straight back through the mirror and beam splitter back to the lamp.  When we do this for both of the mirrors, the path length difference, $d$, causes each mirror to produce its own image of the lamp, $S1$ and $S2$. The result is shown in Figure~\ref{fig:michelson-unwrapped}.

    If we make an observation from the location marked I$_0$ in Figure \ref{fig:michelson-unwrapped}, then the path length difference is $2d$ (remember that the light really travels the path length difference twice). However, if we make an observation from an angle $\theta$, we can see from Figure \ref{fig:michelson-unwrapped} that the path difference between the arms at viewing angle $\theta$ is approximately $2d \cos{\theta}$.
    When the path difference is an integer multiple of the wavelength, we get constructive interference and see a fringe:
    \begin{align}
    m \lambda = 2 d \cos{\theta},
    \end{align}
    where $m$ is the ``order'' of the fringe. Absolute order numbers are typically irrelevant, because it is very difficult to count out from $m=0$ at $d=0$ to whatever $m$ you are at.
    However, \textit{differences} in order number are very useful, and are the primary way of using the interferometer to gather data.

    Note that this analysis \textit{does not} include the $\pi$ phase shift that occurs when light reflects from a material with a higher index of refraction (i.e., a mirror or piece of glass), and it does not account for how your eye focuses the incoming rays so that you can see them.

    Another powerful interferometric technique involves looking at the \textbf{fringe contrast}.
    If two wavelengths of light are passed through the interferometer at the same time they will each produce their own independent fringe pattern.

    Since these fringe patterns are both visible at the same time but do not have the same wavelength, the same order number will not correspond to the same viewing angle.
    At $d=0$, the patterns will line up, since $d=0$ forces $m = 0$ for both patterns.
    However, at $m_1=1$ for the first wavelength, $m_2$ will be ever so slightly different than 1, so the fringes will not line up.
    As $d$ continues to increase the patterns will gradually move out of sync (whenever $m_1 \lambda_1 = \left(m_1+\frac{1}{2}\right) \lambda_2 = 2 d \sin{\theta}$) and back into sync (whenever $m_1 \lambda_1 = (m_1 + 1) \lambda_2 = 2 d \sin{\theta}$).
    We call these out-of-sync regions \textbf{contrast minima} because bright fringes of $\lambda_1$ will be on top of the dark fringes of $\lambda_2$. The in-sync regions are \textbf{contrast maxima}.
    Note that $d=0$ is always a contrast maximum.

    Because the period of this pattern is related to the difference in wavelengths, we can use this effect to measure the \textbf{line splitting}, the difference in wavenumbers ($\sigma = 1/\lambda$) between the two levels.
    To formalize this analysis, suppose we have the interferometer at movable mirror position $d_1$, an interference minimum.
    At this position the order number for the first wavelength is $m$, so for the other it must be $m+\frac{1}{2}$.
    In terms of wavenumbers, we have
    \begin{align}
        2\sigma_1 d_1 \cos{\theta} = m, \qquad 2\sigma_2 d_1 \cos{\theta} = m + \frac{1}{2}.
    \end{align}
    At the next interference minimum, we have
    \begin{align}
        2\sigma_1 d_2 \cos{\theta} = m + \Delta m, \qquad 2\sigma_2 d_2 \cos{\theta} = m + \frac{1}{2} + \Delta m + 1.
    \end{align}
    Subtract:
    \begin{align}
        2\sigma_1 \cos{\theta} \left(d_2-d_1\right) &= N \\
        2\sigma_2 \cos{\theta} \left(d_2-d_1\right) &= N + 1.
    \end{align}
    Subtract again and divide:
    \begin{align}
        2\left(\sigma_1 - \sigma_2\right)\left(d_2-d_1\right) &= 1 \\
        \Delta \sigma = \frac{1}{2 \, \Delta d}.
    \end{align}
    The difference in wavenumber is inversely proportional to the difference in path length difference between two contrast minima (or maxima).
    Note that you cannot extract $\Delta \lambda$ from $\Delta \sigma$ without knowing one of the two wavelengths.

    \section{Equipment}


    For this lab, you will need:
    \begin{itemize}
        \item A pre-assembled Michelson interferometer.
        \item A thin, sharp object, like a tack.
        \item A diffuser plate.
        \item A small diffraction grating.
        \item A red filter.
        \item A sodium lamp.
        \item A hydrogen-deuterium lamp.
        \item A Lens
        \item A screen
    \end{itemize}

    \vspace{1cm}

    \noindent
    There is one key point that you must not forget: the movable mirror \textbf{on some of the setups} (there will be a note attached to them) is attached to a micrometer which controls its distance from the beam splitter via a lever arm.
    \textit{The lever makes the mirror move only one fifth of the distance that you move the micrometer!}
    If you do not account for this, all of your measurements will be off by a factor of 5.


    \FloatBarrier
    \newpage

    \section{Procedure}

    \subsection{Making Crude Measurements}

    \subsubsection{Angular Alignment}

    Before we do anything else, we need to align the interferometer.
    The first step is to make sure that the mirrors are both at the same tilt relative to the path the light takes.
    Put the diffuser between the sodium lamp and the interferometer, and put a thumb tack or other sharp-tipped object in the way so that you see its shape when you look down the interferometer.
    You should see two pairs of the tack's shadow (if you don't see two full pairs then there might be some overlap).
    One of these pairs will move when the tiltable mirror is tilted and the other will not.
    We will use the shadow on the right side of each pair, since that one is typically easier to see.
    These two shadows come from reflections involving the surface of the beam splitter that is away from the lamp.
    The shadows on the left come from the surface that is close to the lamp.

    Once you can see the pair of images on the right, adjust the tilt of the tiltable mirror until the images overlap.
    You should see faint straight-line fringes form in the yellow light from the sodium lamp.
    Remove the sharp object and continue adjusting the tiltable mirror with small motions until you see the fringes ``wrap around'' and form a bullseye pattern.

    \subsubsection{Rough Path Difference Alignment}

    At this point we are most likely far away from zero path difference between the interferometer arms, so we can make large changes.
    Move the movable mirror so that the fringes are moving in or out, such that the path length difference is decreasing (see the first Prelab question).
    Once the fringes become very large in the field of view (only one or two fringes visible), move to the next step (make sure to remember which direction zero path is in).

    \subsubsection{Precise Path Difference Alignment}

    Switch the sodium lamp out for the filament lamp.
    Remove the diffuser.
    Hold the diffraction grating a few inches in front of your eye and look down the interferometer through it.
    You should see a rainbow about $\ang{30}$ off-center.
    This rainbow is the image of the filament, broken up into different colors by the grating, which acts like a prism. Continue moving (now much more slowly) in the direction of zero path.
    When you get very close, dark bands will appear across the rainbow.
    As you get closer and closer the bands will become larger and larger.

    As you keep moving toward zero path, you will eventually see colors (red and blue, and possibly others) appear in your field of view directly over the filament.
    At this point you are within a few microns of zero path. Put the diffuser back in and look down the interferometer without the diffraction grating.
    Tilt the tiltable mirror very slightly so that you get straight-line fringes. Move the movable mirror back and forth very slightly.
    There should be about 10 visible fringes, most having a distinct red-blue tint to the edges, but with one black fringe in the center of the pattern.
    The pattern is symmetric around that center black fringe, so when it is in the center of your field of view the interferometer must be at zero path.

    \checkpoint{
        Align your interferometer and get it to zero path difference.
        \begin{itemize}
            \item Record the micrometer reading at this position so that you can go back to it quickly.
        \end{itemize}
    }

    \subsubsection{Measuring the Wavelength of the Sodium Emission Line}

    We can use the interferometer to measure the wavelength of the sodium lamp by counting fringes.
    Align the interferometer for bullseye fringes. Place the lens at the output of the interferometer and place the screen so that the fringes form an image on it.
    Go to a path difference where you have good fringe contrast and can see many fringes in your field of view.
    Move the micrometer \textbf{slowly} in either direction while watching fringe appear or disappear in the center of the bullseye.
    You will need to count how many fringes pass through the center to make your measurement.
    We recommend taping a piece of paper to the screen and marking your starting fringe.
    You can use the mark to keep track of which fringe you are on.

    Since $\theta=0$ at the center of the bullseye, we have
    \begin{align}
        \lambda = \frac{2 \Delta d}{\Delta m},
    \end{align}
    where $\Delta d$ is the difference in movable mirror position and $\Delta m$ is the number of fringes you count passing through the center.
    Note the extra factor of $5$ introduced by the lever arm!
    (See the equipment section.)
    
    You will want to count as many fringes as possible to reduce error introduced by the micrometer, but not so many that you loose track of what fringe you're on (which is easy to do).
    $\Delta m \approx 50$ is a reasonable tradeoff.

    \checkpoint{
        Measure the wavelength of the sodium emission line by counting fringes.
        \begin{itemize}
            \item Compare your measurement to the expected wavelength ($\lambda \approx \SI{589}{\nano\meter}$).
            \item Why is fringe-counting by eye a bad way to measure wavelengths?
        \end{itemize}
    }

    \subsection{Measuring Sodium Emission Line Splitting}

    There are really two different sodium emission lines near $\SI{589}{\nano\meter}$.
    A pair of emission lines like this is called a \textbf{doublet}.
    Although it is difficult to measure the actual wavelength of those lines (as you have just seen), we can still make a very accurate measurement of the difference in their wavenumbers.

    \checkpoint{
        Measure the wavenumber splitting of the sodium emission lines by counting multiple contrast minima.
        \begin{itemize}
            \item Compare your result to the expected wavenumber splitting ($\Delta \sigma \approx \SI{17.2}{\per\centi\meter}$).
            \item Why is this technique both more accurate and more precise than your measurement of the wavelength by fringe-counting?
        \end{itemize}
    }

    \newpage
    \FloatBarrier

    \section{Prelab Questions}
    \setcounter{subsection}{0}

    \subsection{Fringe Movements}

    Suppose we have the interferometer showing a bullseye interference pattern far away from zero path difference.
    As the path length difference $d$ is decreased, does a given fringe $m$ move in (to smaller $\theta$) or out (to larger $\theta$)?

    \vspace{\stretch{1}}

    \subsection{One Fringe, Two Fringe, Light Fringe, Dark Fringe}

    If the path length difference between the interferometer arms is zero, the light coming from each arm should constructively interfere and produce a bright fringe.
    However, as mentioned in the alignment procedure, the fringe at zero path difference is actually dark.
    One path must get an additional $\pi$ phase shift that the other does not.
    Where and why does this happen?

    \vspace{\stretch{2}}

\end{document}
