\documentclass[]{manual}

\title{Diffraction}
\date{\today}
\course{Physics 325}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section{Motivation}

    One of the most fundamental optical phenomena is \textbf{spatial interference}.
    This type of interference can occur whenever multiple coherent light sources (i.e., phase-stable relative to each other) are imaged at the same location in space.
    Engineered optical setups like the Michelson interferometer let us use spatial interference to measure the microscopic properties of light in well-controlled conditions.
    Advanced optical technologies like thin-film coatings also rely on spatial interference.
    But even the simplest setup can exhibit interference and let us measure the wavelength of light.
    In this lab we will build an intuition for the intensity patterns produced by spatial interference, and use those patterns to measure the wavelength of light.


    \section{Background}

    In this lab we will investigate what is perhaps the most common spatial interference phenomena: \textbf{diffraction}.
    Diffraction occurs when light is forced to travel through an aperture.
    The first aperture we will consider is a \textbf{slit}, a rectangular hole in an otherwise-opaque barrier.
    The situation is illustrated in Figure~\ref{fig:single-slit-diffraction-geometry}.

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \foreach \x in {0, .4, .8}
                \draw[rounded corners] (-3 + \x, 5)
                 -- (-2.8 + \x, 4.5)
                 -- (-2.9 + \x, 4)
                 -- (-2.5 + \x, 3.5)
                 -- (-2.7 + \x, 3)
                 -- (-2.4 + \x, 2.5)
                 -- (-2.6 + \x, 2)
                 -- (-2.5 + \x, 1.5)
                 -- (-2.9 + \x, 1)
                 -- (-2.7 + \x, 0.5)
                 -- (-3 + \x, 0);

            \draw[|<->|, dashed] (-0.3, 2) -- (-0.3, 3) node[midway, left] {$W$};

            \draw[ultra thick] (0, 0) -- (0, 2);
            \draw[ultra thick] (0, 3) -- (0, 5) node[above] {Barrier w/ Slit};

            \draw[dashed] (0, 2.5) -- (8, 2.5) node[midway, below] {$L$};

            \draw[dashed] (0 ,2.5) -- (8, 3.5) node[midway, above] {$r$};

            \draw[|<->|, dashed] (8.2, 2.5) -- (8.2,3.5) node[midway, right] {$x$};

            \draw[<->, dashed] (7, 2.5) arc (0:6.4:7) node[midway, right] {$\theta$};

            \draw[fill] (0, 2.5) circle (2pt);
            \draw (0, 2.5) -- (0.5, 2.25);
            \node[align=center,below] at (1,2.35) {$(z_0, \, 0)$};

            \draw[fill] (0, 2.75) circle (2pt);
            \draw (0, 2.75) -- (0.5, 2.85);
            \node[align=center,above] at (1,2.7) {$(z_0, \, w)$};

            \draw (8, 0) -- ++(0, 5) node[above] {Screen};

            \draw[fill] (8,3.5) circle (2pt) node[above right] {Observation Point};
        \end{tikzpicture}

        \caption{An electromagnetic wave illuminating a single slit.}

        \label{fig:single-slit-diffraction-geometry}
    \end{figure}

    To determine what we see on the screen (i.e., $I(x) = \abs{E(x)}^2 = E(x) \, E^*(x)$, called the ``intensity pattern'' or ``diffraction pattern''), we will use \textbf{Huygen's Principle}, which says that we can treat each point in the plane of the rectangular hole as an independent spherical emitter.
    To determine the electric field anywhere, we sum over the contributions from each point:
    \begin{align}
        E(z, x) &= \int^{W/2}_{-W/2} \dd{w} E(z_0, w) \, \frac{e^{i k r}}{r} = \int^{W/2}_{-W/2} \dd{w} E(z_0, w) \, \frac{e^{i k \sqrt{(w-x)^2 + L^2}}}{\sqrt{(w-x)^2 + L^2}},
    \end{align}
    where $z$ is the coordinate going between the barrier and the screen, $x$ is the distance along the screen to the observed point, $r$ is the distance from some point in the slit to the observed point on the screen,$w$ is the distance of that point to the center of the slit, $W$ is the width of the slit, and $k = 2 \pi / \lambda$ is the wavenumber of the light.

    This integral is not tractable without making some assumptions.
    We will assume that we are in the \textbf{far-field}: $L \gg W$.
    This type of diffraction is called \textbf{Fraunhofer diffraction}.
    We can now approximate the square root in the exponent as
    \begin{align} \label{eqn:fraunhoffer-square-root-approx}
        \sqrt{(w-x)^2 + L^2} = \sqrt{w^2 + x^2 - 2wx + L^2} = r \, \sqrt{1 + \frac{-2wx + w^2}{r^2}} \approx r \qty(1 - \frac{wx}{r^2}),
    \end{align}
    and the square root in the denominator as $\sqrt{(w-x)^2 + L^2} = r$. 
    Now we have
    \begin{align} \label{eqn:fraunhofer-diffraction-integral}
        E(x) &= \frac{e^{i k r}}{r} \, \int^{W/2}_{-W/2} \dd{w} E(z_0, w) \, e^{-i k \frac{x}{r} w}.
    \end{align}
    If we also have $L \gg x$, then $r \approx L$ by the small-angle approximation, and we have
    \begin{align} \label{eqn:fraunhofer-diffraction-integral-with-L}
        E(x) &= \frac{e^{i k L}}{L} \, \int^{W/2}_{-W/2} \dd{w} E(z_0, w) \, e^{-i k \frac{x}{L} w}.
    \end{align}
    The integral in the above equation accounts for the interference of all the point sources in the slit, each with initial amplitude $E(z_0, w)$, at point $x$ on the screen.  The phase factor outside the integral is common to all the point sources and accounts for the phase accumulated from propagating the distance $L$ from the slit to the screen.
    
    Suppose that the incoming electromagnetic wave is a plane wave, so that we can take $E(z_0, w) = E_0$.
    Then
    \begin{align} \label{eqn:single-slit-diffraction}
        E(x) &= E_0 \, W \, \frac{e^{i L r}}{L} \sinc\qty(\pi \, \frac{W}{\lambda L} \, x) \nonumber \\
        I(x) &= \frac{E_0^2 \, W^2}{L^2} \sinc^2\qty(\pi \, \frac{W}{\lambda L} \, x)
    \end{align}
    where $\sinc(y) \equiv \sin(y) / y$, and $\sinc(0) = 1$.
    The intensity pattern is shown in Figure~\ref{fig:single-slit-diffraction-intensity}.
    The zeros of $\sinc^2(y)$ occur at $y = 0, \, \pm \pi, \, \pm 2 \pi, \, \pm 3 \pi, \, \cdots$.
    The positions of the local maxima are not evenly spaced and must be calculated numerically.
    The first few are tabulated to a few digits of precision in Table~\ref{tab:sinc-squared-maxima}.
    Note that most numerical software (Mathematica, MATLAB, NumPy, etc.) uses the \textit{normalized} $\sinc$ function, $\sinc(y) \equiv \sin(\pi y) / \pi y$.
    When in doubt, check the documentation!
    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[<->] (-6.5, 0) -- (6.5, 0) node[below right, black] {$x$};
            \draw[->] (0, 0) node[below] {$x=0$}-- (0, 5.5) node[above right, black] {$I(x)$} ;

            \draw[domain=-6:6, samples=501, very thick] plot (\x, {5*pow(sin(0.5 * pi * \x r) / (0.5 * pi * \x), 2)}); % r means in radians

            \draw[<-] (0.05, 5.05) -- (1, 5.2) node[right] {central (zeroth order) maximum $I_0$};
            \draw[<-] (2.86, 1/3) -- (3.5, 2) node[above] {first order maximum $I_1$};
            \draw[<-] (4.91, 1/5) -- (6, 1) node[above] {second order maximum $I_2$};

            \draw[<-] (-2, 1/10) -- (-3, 3) node[above] {first minimum};
            \draw[<-] (-4, 1/10) -- (-5, 2) node[above] {second minimum};
        \end{tikzpicture}

        \caption{The intensity pattern of single-slit diffraction.}
        \label{fig:single-slit-diffraction-intensity}
    \end{figure}

    \begin{table}
        \centering

        \begin{tabular}{ c c c }
            $n$ & $y_n$ & $\sinc^2(y_n)$ \\
            \hline
            $0$ & $0$ & $1$ \\
            $\pm 1$ & $\pm 1.4303 \, \pi$ & $0.0472$ \\
            $\pm 2$ & $\pm 2.4590 \, \pi$ & $0.0165$ \\
            $\pm 3$ & $\pm 3.4709 \, \pi$ & $0.0083$ \\
        \end{tabular}

        \caption{
            The positions $y_n$ and values of the local maxima of $\sinc^2(y)$.
            Because $\sinc^2(0) = 1$, the last column is also the fractional (i.e., relative) intensity of the maxima relative to the center.
            The approximation $y_n \approx \qty(n + \frac{1}{2}) \, \pi$ is sometimes convenient.
        }

        \label{tab:sinc-squared-maxima}
    \end{table}

    If we have $N$ slits each separated by distance $D$ (center-to-center) instead of just one, we can sum Eq.~\eqref{eqn:single-slit-diffraction} over each slit, with a phase-shift that takes into account the extra translation:
    \begin{align}
        E(x) &= \sum_{n=1}^N E_0 \, W \, \frac{e^{i k L}}{L} \sinc\qty(\pi \, \frac{W}{\lambda L} \, x) \, e^{i \, n \, k \, D \, \frac{x}{L}}.
    \end{align}
    This is a geometric series, which has a closed form sum:
    \begin{align}
        \sum_{n=1}^Nr^n=\frac{r-r^{N+1}}{1-r} \quad \mathrm{for} \quad |r| \leq 1.
    \end{align}
    So we have
    \begin{align} \label{eqn:multi-slit-diffraction}
        E(x) &= E_0 \, W \, \frac{e^{i k L}}{L} \sinc\qty(\pi \, \frac{W}{\lambda L} \, x) \, \frac{e^{i k \, D \, x / L} - e^{i (N + 1) \, k \, D \, x / L}}{1 - e^{i k \, D \, x / L}} \nonumber \\
        I(x) &= \frac{E_0^2 \, W^2}{L^2} \sinc^2\qty(\pi \, \frac{W}{\lambda L} \, x) \, \frac{\sin^2(\pi \frac{N D}{\lambda L} x)}{\sin^2(\pi \frac{D}{\lambda L} x)}.
    \end{align}
    This intensity pattern is shown in Figure~\ref{fig:multi-slit-diffraction-intensity}.
    The overall envelope of multiple lobes comes from the $\sinc^2$ term, and the oscillations under each lobe, which we call sub-lobes in this lab, come from the $\sin^2$ ratio.
    Since the $\sinc^2$ involves only $W$ and the $\sin^2$ involve only $D$, we see that the slit width controls the lobe pattern and the slit spacing controls the sub-lobe pattern.
    The two features are essentially independent of each other.
    We will revisit this perhaps-surprising result when we start thinking about Fourier optics.

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[<->] (-6.5, 0) -- (6.5, 0) node[below right, black] {$x$};
            \draw[->] (0, 0) node[below] {$x=0$}-- (0, 5.5) node[above right, black] {$I(x)$} ;

            \draw[domain=-2.01*pi:2.01*pi, samples=1001, very thick] plot (\x, {1.2 * ((sin(\x r)/\x)*(sin(10*\x r)/sin(5*\x r)))^2)});

            \draw (-pi,0) to [out=-60,in=180] (-pi/10,-0.5) to [out=0, in=90] (0,-1) node[below] {central lobe} to [out=90, in=180] (pi/10,-0.5) to [out=0, in=-120] (pi,0);

            \draw (-2*pi,0) to [out=-60,in=180] (-1.6*pi,-0.15) to [out=0,in=90] (-1.5*pi,-0.5) node[below] {second lobe} to [out=90,in=180] (-1.4*pi,-0.15) to [out=0, in=-120] (-pi,0);


            \draw[<-] (0.05, 4.85) -- (1, 5.2) node[right] {central maximum $I_0$};
            \draw[<-] (pi/5+.05, 4.3) -- (1.25, 4.5) node[right] {first sub-lobe};

            \draw[<-] (-pi, 1/10) -- (-pi, 3) node[above] {first minimum};
            \draw[<-] (-2*pi, 1/10) -- (-2*pi, 2) node[above] {second minimum};
        \end{tikzpicture}

        \caption{The intensity pattern of multiple-slit diffraction.}
        \label{fig:multi-slit-diffraction-intensity}
    \end{figure}

    \section{Equipment}

    For this lab, you will need:
    \begin{itemize}
        \item An optical rail, with several mounting brackets.
        \item A Helium-Neon (He-Ne) laser on a post.
        \item A slit-holder, on a post, mounted on a translation stage.
        \item A set of single and double-slit slides.
        \item Two adjustable slits, on posts.
        \item A photodiode, on a post, mounted on a translation stage.
        \item A multimeter.
    \end{itemize}


    \FloatBarrier
    \newpage

    \section{Procedure}

    \subsection{Single-Slit Diffraction}
    Set up the laser, single-slit holder, and photodiode on the optical rail such that the laser beam passes through one the slits (write down the slit width for that slit) and is diffracted onto the photodiode.
    Hook the photodiode up to the multimeter and observe how the output voltage varies as you change the horizontal and vertical positions of the slit assembly and photodiode (make sure the multimeter is DC-coupled).
    The photodiode output voltage is proportional to the number of photons it is detecting, which is proportional to the light intensity on the detector (plus a small offset, which you must account for).
    Optimize the horizontal and vertical alignments so that you have maximum intensity on the photodiode at the center of the diffraction pattern (i.e., at the position of the central maximum).
    
    Note that, throughout the lab, you may need to switch which set of slits you are using, or how far the slits are from the photodiode, so that you can resolve the features of the diffraction pattern, or have enough horizontal translation to scan across an interesting portion of it.
    Do not use slits that produce diffraction patterns that are either too wide or too narrow to give good results.

    \checkpoint{
        Compare the theoretical prediction for the locations of the maxima in single-slit diffraction (Equation~\eqref{eqn:single-slit-diffraction}) to your real setup.
        \begin{itemize}
        \item Determine the positions of the central and first order maxima and use their separation to determine the wavelength of the laser via Eq.~\eqref{eqn:single-slit-diffraction}.
       	\item Compare your result to the expected wavelength, $\lambda = \SI{632.8}{\nano\meter}$.
        \end{itemize}
    }

	\checkpoint{
    	Now that we know all of the parameters that go into Eq.~\eqref{eqn:single-slit-diffraction}, we can compare the theoretical expression to the experiment results at many positions on the screen.
    	\begin{itemize}
    		\item Make a plot of the theoretical intensity as a function of position on the screen, $I(x)$, using your known $W$ and measured $\lambda$.
    		\item Measure $I(x)$ at evenly spaced positions that are dense enough to resolve the lobes of the diffraction pattern.
    		\item Overlay your measured intensities onto your theory plot.
                How well do they agree?
    	\end{itemize}
	}

    \subsection{Double-Slit Diffraction}
    Switch out the single-slit assembly for a double-slit holder.
    Readjust your setup to make  sure that you can resolve the small features in the double-slit diffraction pattern, including the sub-lobes.

    \checkpoint{
    	Compare the theoretical predictions for the locations of the sub-lobes in double-slit diffraction (Eq.~\eqref{eqn:multi-slit-diffraction}) to your real setup.
    	\begin{itemize}
    		\item Use your known $N$, $D$, $W$, and $\lambda$ to predict the positions of the sub-lobes.
    		\item Measure the positions of the sub-lobes and compare your results to your predictions.
                How well do they agree?
    	\end{itemize}
    }

	\checkpoint{
        Compare the theoretical expression to your experimental results at many positions on the screen.
    	\begin{itemize}
    		\item Make a plot of the theoretical intensity for double-slit diffraction.
    		\item Measure $I(x)$ at evenly spaced positions that are dense enough to resolve the lobes and sub-lobes of the diffraction pattern.
    		\item Overlay your measured intensities onto your theory plot.
                How well do they agree?
    	\end{itemize}
	}

    \subsection{Fresnel Diffraction}
    If we can't neglect the quadratic terms in \eqref{eqn:fraunhoffer-square-root-approx}, we instead get
    \begin{align} \label{eqn:fresnel-square-root-approx}
        \sqrt{(s-x)^2 + L^2} = \sqrt{w^2 + x^2 - 2wx + L^2} = r \, \sqrt{1 + \frac{-2wx + w^2}{r^2}} \approx r \qty(1 - \frac{(w-x)^2}{2r^2}),
    \end{align}
    and the amplitude becomes (after the small angle approximation)
    \begin{align} \label{eqn:fresnel-diffraction-integral}
        E(x) &= \frac{e^{i k L}}{L} \, \int^{W/2}_{-W/2} \dd{w} E(z_0, w) \, e^{-i k \frac{(w-x)^2}{2L}}.
    \end{align}
    For an incoming constant-amplitude plane wave we can pull out the electric field and make a variable transformation $\nu = (w-x) \sqrt{2/(\lambda L)}$.
    This yields
    \begin{align} \label{eqn:fresnel-diffraction}
        E(x) &= E_0 \frac{e^{i k L}}{L} \, \sqrt{\frac{L \, \lambda}{2}} \, \int^{\nu_{\mathrm{max}}}_{\nu_{\mathrm{min}}} \dd{\nu} e^{-i \frac{\pi}{2} \nu^2},
    \end{align}
    where
    \begin{align}
        \nu_{\mathrm{min}} = -\qty(\frac{W}{2} + x) \sqrt{\frac{2}{\lambda L}}
        \qquad \qquad
        \nu_{\mathrm{max}} = \qty(\frac{W}{2} - x) \sqrt{\frac{2}{\lambda L}}.
    \end{align}
    This type of diffraction is called \textbf{Fresnel diffraction}.
    The shape of the resulting intensity pattern is mostly determined by the quantity $\Delta \nu = \nu_{\mathrm{max}} - \nu_{\mathrm{min}}$.

    To observe Fresnel diffraction from a previously-collimated beam, we would need to look in the near field, just after the slit.
    To make it easier to observe using our setup, we will instead send a spherical wave at a slit.
    The Fresnel diffraction will then appear in the far-field instead of the near-field.

    You have been provided with two adjustable slits.
    We will use the first slit to produce a ``spherical wave'' in the near field (it won't really be spherical, but it should look like a very, very wide single-slit diffraction pattern), and the second to produce a Fresnel diffraction pattern which will propagate into the far field.
    Bring the slits as close together as possible, and make the first slit as small as possible (so as to mimic a single point source).
    Adjust the second slit so that the light passes through it.
    The distance between them is then $L$, and the width of the second slit is $W$.

    \checkpoint{
        Create and observe the Fresnel diffraction pattern for $\Delta \nu = 3.9$ on your screen.
        It should appear to be a broad blob with the center slightly dimmer than the edges.
    }

    \newpage
    \FloatBarrier

    \section{Prelab Questions}
    \setcounter{subsection}{0}

    \subsection{Wavelength from Intensity Maxima}

    Consider the intensity pattern produced by monochromatic light undergoing Fraunhofer diffraction from a single slit.
    Derive an algebraic (i.e., not transcendental) relationship between the location of the first-order intensity maximum and the wavelength of the light.
    Do the same for the second maximum, the first minimum, and the second minimum.

    \vspace{\stretch{1}}

    \subsection{Insert Pun Here}
    
    In single-slit diffraction, if the slit width is increased, do the lobes in the diffraction pattern get closer together or farther apart?
    In double-slit diffraction, if the slit spacing is decreased, do the sub-lobes get closer together or farther apart?
    
    \vspace{\stretch{1}}

\end{document}
