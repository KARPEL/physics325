\documentclass[]{manual}

\title{Acousto-Optic Modulators}
\date{\today}
\course{Physics 325}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section*{Motivation}

    Acousto-optic modulators (AOMs) are an extremely useful piece of optics lab equipment.
    One of the most common problems in the lab is getting your laser to the desired frequency.
    There are far fewer narrow, high-power laser frequencies than there are atomic and molecular transitions that we want to target.
    Some lasers are widely tunable, but they may not be very efficient over their entire bandwidth.
    Even worse, what if we want to target two atomic transitions that are very close to each other at the same time?
    Do we need to get two identical lasers and tune them ever-so-slightly differently?

    Luckily, we don't.
    AOMs can solve these problems, and in solving them, also provide other useful capabilities.
    In this lab, we will explore both the solution to the problems stated above and other uses of AOMs.

    \section*{Background}

    The AOM is a small crystal attached to a piezo-electric transducer.
    When a voltage is applied to the piezo, it expands and pushes on the crystal, producing a sound wave.
    By applying an oscillating voltage from a radio-frequency (RF) source, we can make the piezo oscillate.
    The index of refraction is dependent on the density of atoms in the material.  Since the sound waves compress and rarefy the crystal, the crystal will have an index of refraction that varies over time at ``electronic'' frequencies (\si{\mega\hertz} or \si{\giga\hertz}).
    When light passes through this crystal, it will gain additional frequency components, called \textbf{sidebands}, separated from the original frequency by integer multiples of whatever \textbf{modulation frequency} the crystal is driven at.

    Alternatively, one can think of the light as diffracting off of a sound wave in the crystal, or of the photons scattering off of phonons.
    Any of these pictures work, but for this lab, the important result is the angle that the diffracted light exits the crystal at:
    \begin{align} \label{eqn:output-angle}
        \Delta \theta = \frac{\lambda}{v} \, \Delta f,
    \end{align}
    where $\lambda$ is the wavelength of the light in vacuum, $\Delta f$ is the modulation frequency, and $v$ is the speed of sound in the crystal.
    Each sideband will come out of the AOM deflected by an angle $n \, \Delta \theta$ with frequency $f_0 + n \, \Delta f$, where $f_0$ is the original frequency.
    The integer $n$ is the \textbf{order number}.
    Note that $n$ may be negative!
    Multiple orders in both directions may be visible.
    Most AOMs are engineered so that most of the power goes into the $+1$ or $-1$ order.

    The geometry of the laser-to-AOM setup that you will use in this lab is illustrated in Figure~\ref{fig:aom-setup}.

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[thick, fill=gray!30] (-1, -0.5) rectangle ++(2, 1) node[midway] {Laser};

            \draw[pattern = north west lines, rotate around = {-45:(3, 0)}] (3, -.5) rectangle ++(.1, 1) node[midway, below right] {M1};
            \draw[pattern = north west lines, rotate around = {-45:(3, 2.75)}] (3, 2.25) rectangle ++(-.1, 1) node[midway, above left] {M2};

            \draw[fill=gray!20] (5, 2) rectangle ++(2, 2);
            \draw[thick,fill=gray!40] (5, 1) rectangle ++(2, 1) node[midway] {\small Transducer};
            \draw[thick,fill=gray!40] (5, 4) rectangle ++(2, .2) node[midway, above] {AOM};

            \draw[veryneararrow={stealth}, midarrow={stealth}] (1, 0) -- (3, 0) -- (3, 2.75) -- (6.5, 2.65);
            \foreach \t in {-3,-2,...,3} {
                \draw[veryfararrow={stealth}] (6.5, 2.65) -- ++({\t*15}:3) node[right] {\t};
            }
            \draw[<->, dashed] (8.5, 2.65) arc (0:15:2) node[midway, right] {$\Delta \theta$};

            \draw[ultra thick] (6, .5) -- (6, 1);
            \draw[thick, fill=gray!30] (5, -0.5) rectangle ++(2, 1) node[midway] {RF Source};
        \end{tikzpicture}

        \caption{
            An acousto-optic modulator, with input laser (guided by mirrors M1 and M2) and output beams.
            The modulator's transducer is connected to a radio-frequency (RF) source and the acoustic waves travel up the page, roughly perpendicularly to the optical beam.
            Note that the beam going into the AOM enters into the half of the crystal closer to the transducer.  Also note that the beam enters the crystal at a slight angle.
        }

        \label{fig:aom-setup}
    \end{figure}

    \FloatBarrier
    \section*{Equipment}

    For this lab, you will need:
    \begin{itemize}
        \item An acousto-optic modulator, with mount.
        \item A variable-frequency, low-power radio-frequency (RF) source.
        \item A fixed-frequency, high-power radio-frequency (RF) source.
        \item A photodiode, with mount.
        \item An oscilloscope.
        \item A Helium-Neon (He-Ne) laser, with mount.
        \item Two mirrors, with mounts, and whatever other optics you want for coupling into the AOM and differentiating the output beams.
    \end{itemize}

    \noindent
    \textbf{Never run the RF sources without a load!} Running the sources without loads can severely damage them.

    \FloatBarrier
    \newpage

    \section*{Procedure}
    
    \subsection{Optimizing the AOM Alignment}
    
    To get a coarse alignment, pass the beam through the AOM without the AOM being screwed down yet.
    Turn and translate the AOM very slightly by hand.
    You'll need to have the AOM running while doing this.
    When you see the modulated orders coming out of the AOM (they might be dim), you're close enough to lock down the AOM position.
    Then you can optimize the output power by walking the input beam and the fine-angle adjustment on the AOM mount.

    
    \checkpoint{
        Maximize the power in the $+1$ order with the fixed-frequency RF source by aligning the laser through the AOM.
    }

    \subsection{Measuring $\theta$}

    Our first goal is to measure $\Delta \theta$ as a function of modulation frequency to confirm \eqref{eqn:output-angle}.
    
    You may notice that the brightness of the output light varies as you change the frequency.
    We'll investigate this in the next section.

    \checkpoint{
        Measure the deflection angle as a function of modulation frequency using the variable-frequency RF source.
        \begin{itemize}
            \item Make a plot of deflection angle vs. modulation frequency for the $+1$ order.
            Overlay the expected theoretical result from \eqref{eqn:output-angle}.
            Do they agree?
        \end{itemize}
    }

    \subsection{Measuring the Bandwidth}

    Let's investigate how the output power varies with frequency using the photodiode.
    Since you'll need to move the photodiode between measurements, you'll need to be careful about background light.
    You may also need to figure out a way to minimize the amount of light coming from the $0$ order, depending on how much power you were able to push into the $+1$ order.

    \checkpoint{
        Measure the output power as a function of frequency using the variable-frequency RF source.
        \begin{itemize}
            \item Make a plot of frequency vs. output power for the $+1$ order.
            \item For what frequency is the output power maximized?
            \item What is the bandwidth of the AOM (full width at the half-power point)?
        \end{itemize}
    }

    \subsection{Rise Time}

    Another important property of the AOM is its \emph{rise time}:
    how long does it take for the output power to stabilize after turning the device on?
    AOMs are often used to control the timing of when a beam is on or off, a process known as gating.  In order to properly gate a beam the rise time must be known.

    Set up a photodiode to measure the power from the $+1$ order.
    Set up the oscilloscope to trigger a single shot from a rising voltage and hook it up to the photodiode.
    Use the high-power, fixed-frequency source for this part of the experiment.

    \checkpoint{
        Turn the RF source off and on to trigger the oscilloscope and observe the rise of the $+1$ order.
        \begin{itemize}
            \item Roughly how long does it take the AOM to turn on?
            \item What shape is the curve? 
                What quantity corresponds to the rise time?
        \end{itemize}
    }

    \FloatBarrier
    \newpage

    \section*{Prelab Questions}
    \setcounter{subsection}{0}

    \subsection*{Frequency Modulation}

    An important aspect of experimental science is using equipment manuals to understand how you should expect your equipment to work.
    Look up the manual for the AOM we will be using in lab: Gooch \& Housego R23080-3-LTD.
    PDFs of the manual are readily available online.
    Use the specifications and calculations sections to answer the following questions:
    \begin{itemize}
    	\item What is the interactive material of the AOM?
    	\item What is the speed of sound in this material?
    	\item If we send a \SI{500}{\micro\meter} diameter beam through the AOM, what is the expected rise time?
    \end{itemize}

    \vspace{\stretch{1}}

\end{document}
