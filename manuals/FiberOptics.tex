\documentclass[]{manual}

\title{Fiber Optics}
\date{\today}
\course{Physics 325}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section*{Motivation}

    Fiber optics are an important, and still increasingly important, tool in both physics and engineering.
    Fibers (long, thin pieces of glass) can be used to transport light over great distances.
    They are already extremely important in the communications industry, because a fiber-optic cable can carry much more information per second than a traditional metal wire can.
    In this lab we will practice ``coupling'' to a fiber, the process of aligning and focusing a laser beam so that most of its power ends up traveling along the fiber.

    \section*{Background}

    Fibers are long cylinders, typically made from two layers of fused-silica glass.
    The inner layer (the \textbf{core}) has a slightly larger refractive index than the outer layer (the \textbf{cladding}) (i.e., $n_{\mathrm{core}} > n_{\mathrm{cladding}}$).

    A simplified model of the operation of a fiber can be derived from ray optics.
    As a light ray propagates down the fiber, it will eventually hit the boundary between the core and the cladding.
    Because the core has a slightly larger index of refraction, the light can undergo total internal reflection if the impact angle is shallow enough.
    Rays that satisfy this condition stay in the fiber over long distances, undergoing total internal reflection many times.

    The two most important quantities to consider when choosing a fiber (besides the internal losses) are the numerical aperture (NA) and the $V$ parameter.
    The numerical aperture has the same meaning as it did for lenses, and is roughly the opening angle of the cone of light that comes out of the fiber.
    It is given by
    \begin{align}
        \mathrm{NA} = \sqrt{n_{\mathrm{core}}^2 - n_{\mathrm{cladding}}^2}.
    \end{align}
    The $V$ parameter determines the number of \textbf{spatial modes} that can propagate in the fiber.
    This comes from a more accurate model than the ray optics model, where we consider what kind of electromagnetic modes can propagate down a cylinder.
    It is given by
    \begin{align}
        V = k \, a \, \mathrm{NA},
    \end{align}
    where $a$ is the radius of the core and $k = 2\pi / \lambda$ is the free-space wavenumber of the incident light.

    Broadly, there are two classes of fiber-optic cables: single-mode and multi-mode.
    Single-mode fibers have $V \lessapprox 2.405$ and support one or only a very small number of spatial modes.
    Multi-mode fibers, on the other hand, can support hundreds of modes.
    The main difference between them is that the core radius $a$ is much larger for a multi-mode fiber.
    For visible light, the typical core radius for a single-mode fiber is a few microns (just a few wavelengths).
    A multi-mode fiber for the same wavelength might have a core radius of more than $\SI{100}{\micro\meter}$.

    You can measure how well-coupled your input laser beam is to the fiber by measuring the \textbf{coupling efficiency}, which is the ratio of the propagating power in the fiber to the input power of the laser beam.
    If the fiber has low internal loss, we can get a reasonable estimate of the propagating power by measuring the output power.

    \section*{Equipment}

    For this lab, you will need:
    \begin{itemize}
        \item A He-Ne laser, with mount.
        \item Two mirrors on mounts, with posts.
        \item A telescope.
        \item A fault-finder.
        \item A fiber-optic coupler mount, with post.
        \item A power meter.
        \item A single-mode fiber, and a multi-mode fiber.
    \end{itemize}


    \FloatBarrier
    \newpage

    \section*{Procedure}

    \subsection{Multi-Mode Fiber}

    Aligning to a multi-mode fiber is much easier than aligning to a single-mode fiber.
    Because the core radius is large and therefore supports many modes, the coupling efficiency is not very sensitive to the size or shape of the input beam.

    \begin{enumerate}
        \item Do a coarse alignment: get the beam going toward the fiber coupler by using the two mirrors to perform a \textbf{beam walk}.
        \item Connect your multi-mode fiber to the fiber connector and connect the other end to the fault-finder.
        The fault-finder has a laser with a pre-aligned fiber-optic connector inside it, so when you turn it on you should see light coming out of the other end of your fiber.
        \item Keep walking your beam until the laser beam overlaps the beam coming out of the fault-finder.
        You'll want to check the overlap at the laser and at the fiber coupler, while adjusting the mirror furthest from where you are checking.
        Because the whole setup is invertible, this should get you very close to aligned (i.e., if the fault-finder beam goes to the laser from the fiber, then the laser will go to the fiber).
        \item Remove the fault-finder and observe the output of the fiber.
        Hopefully, there's some light coming out from your laser.
        If not, put the fault-finder back in and keep walking the beam.
        \item Once you have some output light, shine it on a photodiode and keep walking the beam to maximize the output power.
        You shouldn't have to make very large adjustments at this point!
        You may also need to adjust the position of the fiber coupler lens.
    \end{enumerate}

    \checkpoint{
        Couple your laser beam to the multi-mode fiber.
        \begin{itemize}
            \item Maximize and record the coupling efficiency.
                You should be able to achieve at least 80\% coupling.
        \end{itemize}
    }
    
    \checkpoint{
        Observe the output of the fiber about two meters away from the end of the fiber.
        \begin{itemize}
            \item Is the output beam Gaussian?
                Explain your observations.
        \end{itemize}
    }

    Now that we have some experience aligning fibers, we can test the reasonable thought that aligning to a multi-mode fiber shouldn't depend much on the size or shape of the input beam.
    This is because the multi-mode fiber accepts many input spatial modes (like a poorly-centered or badly-angled Gaussian, for example).

    \checkpoint{
        Add a telescope after the laser to double the radius of the input beam, then realign your setup.
        \begin{itemize}
            \item Maximize and record the coupling efficiency.
                Were you able to achieve similar efficiency?
        \end{itemize}
    }

    \subsection{Single-Mode Fiber}

    Aligning to the single-mode fiber is significantly more difficult than aligning to the multi-mode fiber.
    You will likely not have any visible output after doing the rough alignment with the fault-finder, and will need to rely entirely on the photodiode.

    \checkpoint{
        Couple your laser beam to the single-mode fiber.
        \begin{itemize}
            \item Maximize and record the coupling efficiency.
                You should be able to achieve at least 50\% coupling.
        \end{itemize}
    }
    
    \checkpoint{
        Observe the output of the fiber about two meters away from the end of the fiber.
        Measure the divergence angle of the output light (i.e., the numerical aperture of the fiber).
        \begin{itemize}
            \item Is the output beam Gaussian?
                Explain your observations.
            \item Look up the specification sheet for the fiber online.
                Does your measurement match what the sheet says it should be?
        \end{itemize}
    }

    Since the core diameter of a single-mode fiber is small, the coupling efficiency should be very sensitive to the size of the input beam.

    \checkpoint{
        Add a telescope after the laser to double the radius of the input beam, then realign your setup.
        \begin{itemize}
            \item Maximize and record the coupling efficiency.
            \item Were you able to achieve similar efficiency?
        \end{itemize}
    }

    \FloatBarrier
    \newpage

    \section*{Prelab Questions}
    \setcounter{subsection}{0}

    \subsection*{Calculating $V$}

    Look up the datasheets for three different commercial fiber-optic cables online (Thorlabs and Edmund Optics both sell a wide variety of fibers).
    Calculate the $V$ parameter for each fiber at $\lambda = \SI{632}{\nano\meter}$, given the information in the datasheet (don't make it too hard: use the information they're providing).

    \vspace{\stretch{1}}

\end{document}
