\documentclass[]{manual}

\usepackage{lenses}

\title{Thin Lenses}
\date{\today}
\course{Physics 325}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section*{Motivation}

    The traditional starting point for optics is the study of \textbf{thin lenses}.
    Under the approximations of ray optics, a thin lens bends light in very predictable ways.
    Thin lenses are important in optics labs, where they are used to guide light between different parts of the experiment and to shape laser beams into the necessary forms.
    In this lab we will study the properties of thin \textbf{converging} (positive focal length) and \textbf{diverging} (negative focal length) lenses.

    \section*{Background}

    For rays passing through a thin lens, the distance of the image $s_{\mathrm{i}}$ is determined from the position of the object $s_{\mathrm{o}}$ via the lens equation,
    \begin{align} \label{eqn:lens-equation}
        \frac{1}{s_0} + \frac{1}{s_{\mathrm{i}}} = \frac{1}{f},
    \end{align}
    where $f$ is the \textbf{focal length} of the lens.
    These distances are measured along the \textbf{optical axis} (OA) from the \textbf{principal plane} (PP), in the center of the lens.
    Because the lens is thin, we can think of the refraction as occurring only at the principal plane.
    The two points distance $f$ along the optical axis are called the \textbf{principal foci}.
    We often label them with subscripts to indicated whether they're on the ``object'' side of the lens (o) or the image side of the lens (i), just like the subscripts for $s$.

    Determining $s_{\mathrm{i}}$ is only half of the battle if we want to know where the image is.
    We also need to know its ``height'' above the optical axis.
    Consider the three principal rays shown in Figure~\ref{fig:thin-lens}.
    The rays are defined like this:
    \begin{enumerate}
        \item A ray that connects the principal plane to the object at a right angle, then passes through the principal focus on the image side.
        \item A ray that passes through the center of the lens without changing angle.
        \item A ray that connects the object to the principal plane through the principal focus on the object side, then leaves the principal plane at a right angle.
    \end{enumerate}
    The image will form at the intersection of these three rays.
    Only two need to be drawn to locate the image in two dimensions.
    Note that these are not literally the paths of the principal rays through the system, but only a (good) approximation for a very thin lens.

    There are several similar triangles formed by the object and image and the various distances, so we have
    \begin{align}
        \frac{y_{\mathrm{o}}}{\abs{y_{\mathrm{i}}}} = \frac{f}{x_{\mathrm{i}}} = \frac{x_{\mathrm{o}}}{f} = \frac{s_{\mathrm{o}}}{s_{\mathrm{i}}}.
    \end{align}
    The middle relationship leads to
    \begin{align}
        \frac{f}{x_{\mathrm{i}}} &= \frac{x_{\mathrm{o}}}{f} \nonumber \\
        x_{\mathrm{o}} \, x_{\mathrm{i}} &= f^2,
    \end{align}
    which is Newton's form of the lens equation.
    The outer relationship leads to a definition of the \textbf{magnification},
    \begin{align} \label{eqn:magnification}
        m = \frac{y_{\mathrm{i}}}{y_{\mathrm{o}}} = - \frac{s_{\mathrm{i}}}{s_{\mathrm{o}}}.
    \end{align}
    Note how the sign works: if $s_{\mathrm{o}}$ and $s_{\mathrm{i}}$ are both positive (i.e., they are on opposite sides of the lens), then the magnification is \textit{negative}, and the image is inverted relative to the object ($y_{\mathrm{i}}$ has the opposite sign of $y_{\mathrm{o}}$).

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[dashed] (-6, 0) -- (6, 0) node[left, at start] {$\mathrm{OA}$};

            \clens{0}{20}{4}{0.05}

            \draw[fill] (2, 0) circle (2pt) node[above right] (PFI) {$\mathrm{PF_i}$};
            \draw[fill] (-2, 0) circle (2pt) node[above right] (PFI) {$\mathrm{PF_o}$};

            \draw[<->|, dashed] (0, -.7) -- ++(2, 0) node[midway, below] (pfi) {$f$};
            \draw[<->|, dashed] (0, -.7) -- ++(-2, 0) node[midway, below] (pfo) {$f$};
            \draw[<->|, dashed] (2, -.7) -- ++(2, 0) node[midway, below] {$x_{\mathrm{i}}$};
            \draw[<->|, dashed] (-2, -.7) -- ++(-2, 0) node[midway, below] {$x_{\mathrm{o}}$};
            \draw[<->, dashed] (0, 2.2) -- ++(-4, 0) node[midway, above] {$s_{\mathrm{o}}$};
            \draw[<->, dashed] (0, -2.2) -- ++(4, 0) node[midway, below] {$s_{\mathrm{i}}$};
            \draw[<->|, dashed] (-4.4, 0) -- ++(0, 2.8) node[midway, left] {$y_{\mathrm{o}}$};
            \draw[<->, dashed] (4.4, 0) -- ++(0, -2.8) node[midway, right] {$y_{\mathrm{i}}$};

            \object{-4}{3}
            \draw[-{squareTip[scale=3]}] (4, 0) -- ++(0, -3) node[below] {Image};

            \draw[veryneararrow={stealth}, veryfararrow={stealth}, shorten >= -3.8cm] (-4, 2.8) -- (0, 2.8) -- (2, 0);
            \draw[neararrow={stealth}, shorten >= -.4cm] (-4, 2.8) -- (4, -2.8);
            \draw[veryneararrow={stealth}, veryfararrow={stealth}, shorten >= -1cm] (-4, 2.8) -- (0, -2.8) -- (4, -2.8);

            \draw (0, 2.7) -- ++(-.1, 0) -- ++(0, .1);
            \draw (0, -2.7) -- ++(.1, 0) -- ++(0, -.1);
        \end{tikzpicture}

        \caption{
            The thin lens optical system.
        }

        \label{fig:thin-lens}
    \end{figure}

    \section*{Equipment}

    For this lab, you will need:
    \begin{itemize}
        \item A two-meter optical rail.
        \item A converging thin lens.
        \item A diverging thin lens.
        \item Two object projectors (one creates the object, the other is inverted and picks-off the image).
        \item A lamp.
        \item A collimating lens for the object projector.
        \item A ground-glass screen.
        \item A ruler and/or calipers.
    \end{itemize}


    \FloatBarrier
    \newpage

    \section*{Procedure}

    \subsection{Object Projector Setup}

    The core device in this experiment an \textbf{object projector}, which will let you generate an ``object'' in the air without the need of a physical object placed at that location.
    This will let us do things like smoothly move the object through the lens.
    We will view images on a small disk of ground glass.

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[ultra thick] (-3, 0) -- ++(0, -4) -- ++(6, 0) -- ++(0, 4) node[circle, fill, inner sep=2pt] {};
            \draw[ultra thick] (-1.5, 0) -- ++(0, -2.5) -- ++(3, 0) -- ++(0, 2.5) node[circle, fill, inner sep=2pt] {};
            \draw[ultra thick] (-.75, 0) -- ++(0, -1.5) -- ++(1.5, 0) -- ++(0, 1.5) node[circle, fill, inner sep=2pt] {};
        \end{tikzpicture}

        \caption{
            The projected object that we will be imaging.
        }

        \label{fig:projected-object}
    \end{figure}

    Here's the procedure to set up the imaging system on the optical rail and confirm that they are working as intended:
    \begin{enumerate}
        \item Attach the lamp and projector to the optical rail and plug the lamp into the power supply.
        \item Attach the ground-glass disk to the rail and move it until you see the object shown in Figure~\ref{fig:projected-object} appear on it in sharp focus.
        This is the location of the image.
        Move the metal rod so that it can be used to indicate the position of the object during an experiment (since the ground-glass disk will need to be elsewhere).
        \item Attach the thin converging lens to the optical rail about six inches past the end of the metal rod.
        The image of the original object will now act as a new object for the converging lens.
        An image should form in the air on the far side of the lens: locate it using the ground-glass disk.
        \item Remove the ground-glass disk.
        Attach the image pick-off (same device as the object projector, but facing the opposite way) to the rail and move it until the image forms on the plastic screen on the back of the tube.
        \item Remove the converging lens and move the image pick-off until a sharp image appears on the plastic screen.
        Adjust the metal rod on the image pick-off until its end is at the same position as the end of the rod from the object projector.
    \end{enumerate}

    \subsection{Converging Lens}

    \subsubsection{Measuring the Focal Length}

    One way to measure the focal length of a thin lens is to find the distance where incoming parallel rays converge to a single point after passing through it, as shown in Figure~\ref{fig:object-at-infinity}.
    Slip the collimating lens over the object projector to collimate it (move the image out to infinity).
    Attach the converging lens to the rail and move the ground-glass screen back and forth behind it to determine where the image appears.

    \checkpoint{
        Measure the focal length of your thin converging lens using an object at infinity.
    }

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[dashed] (-6, 0) -- (4, 0) node[left, at start] {$\mathrm{OA}$};

            \clens{0}{10}{2}{0.05}

            \foreach \y in {-1.5,-.75,...,1.5}
                \draw[veryneararrow={stealth}, veryfararrow={stealth}] (-5, \y) -- (0, \y) -- (3, 0);

            \draw[fill] (3, 0) circle (2pt) node[above right] (PFI) {$\mathrm{PF_i}$};
            \node[above right] (object) at (-5, 1.6) {Rays from Object at $\infty$};
        \end{tikzpicture}

        \caption{
            Imaging an object at infinity with a thin converging lens.
        }

        \label{fig:object-at-infinity}
    \end{figure}

    \subsubsection{Real Object Magnification}

    Remove the collimating lens.
    Place the object at distance $s_{\mathrm{o}} = \frac{3}{2} f$, as shown in Figure~\ref{fig:3/2f}.

    \checkpoint{
        Investigate the properties of the image in this regime.
        \begin{itemize}
            \item Measure the location of the image $s_{\mathrm{i}}$.
                Compare your result to the expected position (Equation \eqref{eqn:lens-equation}).
            \item Measure the magnification $m$.
                Compare your result to the expected magnification (Equation \eqref{eqn:magnification}).
            \item Is the image real or virtual?
                Is it upright or inverted?
        \end{itemize}
    }

    \checkpoint{
        Determine the object position that gives $m = -1$.
        \begin{itemize}
            \item Measure the location of the image $s_{\mathrm{i}}$.
                Compare your result to the expected position.
            \item Measure the magnification $m$.
                Is it really $-1$?
            \item Does the magnification increase or decrease if you move the lens further away from the object?
                Is $\abs{m} = 1$ the minimum magnification?
        \end{itemize}
    }

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[dashed] (-5, 0) -- (4, 0) node[left, at start] {$\mathrm{OA}$};

            \clens{0}{10}{2}{0.05}

            \object{-3}{1}

            \draw[|<->, dashed] (-3, -.5) -- (0, -.5) node[midway, below] {$\frac{3}{2} f$};
            \draw[<->|, dashed] (0, -.5) -- (2, -.5) node[midway, below] {$f$};

            \draw[fill] (-2, 0) circle (2pt) node[above] {$\mathrm{PF_o}$};
            \draw[fill] (2, 0) circle (2pt) node[above] {$\mathrm{PF_i}$};
        \end{tikzpicture}

        \caption{
            Imaging a real object at distance $s_{\mathrm{o}} = \frac{3}{2}f$ with a thin converging lens.
        }

        \label{fig:3/2f}
    \end{figure}

    \subsubsection{Real Object Within $f$}

    Place the object at distance $s_{\mathrm{o}} = \frac{2}{3} f$, as shown in Figure~\ref{fig:2/3f}.
    Since the image is now virtual, you will need to use the image pick-off to see it.

    \checkpoint{
        Investigate the properties of the image in this regime.
        \begin{itemize}
            \item Measure the location of the image $s_{\mathrm{i}}$.
                Is it where you expected it to be?
            \item Measure the magnification $m$.
                Is it what you expected it to be?
            \item Is the image upright or inverted?
        \end{itemize}
    }

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[dashed] (-5, 0) -- (4, 0) node[left, at start] {$\mathrm{OA}$};

            \clens{0}{10}{2}{0.05}

            \object{-2}{1}

            \draw[|<->, dashed] (-2, -.5) -- (0, -.5) node[midway, below] {$\frac{2}{3} f$};
            \draw[<->|, dashed] (0, -.5) -- (3, -.5) node[midway, below] {$f$};

            \draw[fill] (-3, 0) circle (2pt) node[above] {$\mathrm{PF_o}$};
            \draw[fill] (3, 0) circle (2pt) node[above] {$\mathrm{PF_i}$};
        \end{tikzpicture}

        \caption{
            Imaging a real object at distance $s_{\mathrm{o}} = \frac{2}{3}f$ with a thin converging lens.
        }

        \label{fig:2/3f}
    \end{figure}


    \subsubsection{Virtual Object}

    Since we are projecting the object instead of using a physical object, we can freely push it through the lens.
    Move the object to $s_{\mathrm{o}} = -2f$, as shown in Figure~\ref{fig:-2f}.

    \checkpoint{
        Investigate the properties of the image in this regime.
        \begin{itemize}
            \item Measure the location of the image $s_{\mathrm{i}}$.
                Is it where you expected it to be?
            \item Measure the magnification $m$.
                Is it what you expected it to be?
            \item Is the image real or virtual?
                Is it upright or inverted?
        \end{itemize}
    }

    \begin{figure}
        \centering

        \begin{tikzpicture}
            \draw[dashed] (-5, 0) -- (4, 0) node[left, at start] {$\mathrm{OA}$};

            \clens{0}{10}{2}{0.05}

            \object{4}{1}

            \draw[|<->, dashed] (-2, -.5) -- (0, -.5) node[midway, below] {$f$};
            \draw[<->|, dashed] (0, -.5) -- (4, -.5) node[midway, below] {$2f$};

            \draw[fill] (-2, 0) circle (2pt) node[above] {$\mathrm{PF_o}$};
            \draw[fill] (2, 0) circle (2pt) node[above] {$\mathrm{PF_i}$};
        \end{tikzpicture}

        \caption{
            Imaging a virtual object at distance $s_{\mathrm{o}} = -2f$ with a thin converging lens.
        }

        \label{fig:-2f}
    \end{figure}

    \subsection{Diverging Lens}

    From the point of view of Equation~\eqref{eqn:lens-equation}, the only difference between a thin converging lens and a \textbf{thin diverging lens} is that the focal length of the diverging lens is negative.
    This forces a sign flip for $s_{\mathrm{o}}$ and $s_{\mathrm{i}}$, so we can think of the diverging lens as the ``conjugate'' of the converging lens: real becomes virtual, and virtual becomes real.
    The magnification should stay the same because it depends only on the ratio of $s_{\mathrm{o}}$ and $s_{\mathrm{i}}$, which won't change if they only change sign.
    Therefore, we will repeat the experiments of the preceding section, but swapping real and virtual throughout.
    Note that we keep all of the same notation: which side of the lens is the ``object'' side, which side is the ``image'' side, etc.

    \subsubsection{Measuring the Focal Length}

    Use the collimating lens to move the object out to infinity, as shown in Figure~\ref{fig:object-at-infinity-diverging}.

    \checkpoint{
        Measure the focal length of your diverging lens using an object at infinity.
    }

    \begin{figure}[H]
        \centering

        \begin{tikzpicture}
            \draw[dashed] (-6, 0) -- (4, 0) node[left, at start] {$\mathrm{OA}$};

            \dlens{0}{10}{2}{.4}

            \draw[fill] (3, 0) circle (2pt) node[above right] (PFI) {$\mathrm{PF_i}$};

            \draw[fill] (-3, 0) circle (2pt)
            node (PFO) {}
            node[above left] {$\mathrm{PF_o}$};

            \foreach \y in {-1.5,-.75,...,1.5} {
                \draw[veryneararrow={stealth}] (-5, \y) -- (0, \y);
                \draw[dashed] (-3, 0) -- (0, \y);
                \draw[fararrow={stealth}] (0, \y) -- ++({atan(\y/3)}:2);
            }

            \node[above right] (object) at (-5, 1.6) {Rays from Object at $\infty$};
        \end{tikzpicture}

        \caption{
            Imaging an object at infinity with a thin diverging lens.
        }

        \label{fig:object-at-infinity-diverging}
    \end{figure}

    \subsubsection{Real Object Magnification}

    Create a real object at $s_o = \frac{3}{2} \abs{f}$.

    \checkpoint{
        Investigate the properties of the image in this regime.
        \begin{itemize}
            \item Measure the location of the image $s_{\mathrm{i}}$.
                Is it where you expected it to be?
            \item Measure the magnification $m$.
                Is it what you expected it to be?
            \item Is the image real or virtual?
                Is it upright or inverted?
            \item Move the object closer to the focal point.
                What happens?
        \end{itemize}
    }

    \newpage
    \section*{Prelab Questions}
    \setcounter{subsection}{0}

    \subsection*{The Limits of Minimagnification}

    Sketch a plot of the magnification $m$ vs. object distance $s_{\mathrm{o}}$ for fixed $f>0$, for all $s_{\mathrm{o}}$ from $\infty$ to $-\infty$.
    You may find it helpful to put positive $s_{\mathrm{o}}$ on the left (instead of the right, as is traditional) to match the way we typically draw lens diagrams.
    Do the same for $f<0$.

    \vspace{\stretch{1}}

\end{document}
